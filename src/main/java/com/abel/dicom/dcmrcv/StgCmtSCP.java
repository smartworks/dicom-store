package com.abel.dicom.dcmrcv;

import java.io.IOException;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.UID;
import org.dcm4che2.net.Association;
import org.dcm4che2.net.CommandUtils;
import org.dcm4che2.net.service.DicomService;
import org.dcm4che2.net.service.NActionSCP;

/**
 * @author Gunter Zeilinger <gunterze@gmail.com>
 * @version $Rev$ $Date:: 0000-00-00 $
 * @since Mar 22, 2010
 */
class StgCmtSCP extends DicomService implements NActionSCP {

    private final DcmRcv dcmrcv;

    public StgCmtSCP(DcmRcv dcmrcv) {
        super(UID.StorageCommitmentPushModelSOPClass);
        this.dcmrcv = dcmrcv;
    }

    @Override
    public void naction(Association as, int pcid, DicomObject rq,
            DicomObject info) throws IOException {
        DicomObject rsp = CommandUtils.mkRSP(rq, CommandUtils.SUCCESS);
        dcmrcv.onNActionRQ(as, rq, info);
        as.writeDimseRSP(pcid, rsp);
    }

}

