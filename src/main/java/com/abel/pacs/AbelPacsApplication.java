package com.abel.pacs;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import edu.mayo.qia.pacs.Metrics;
import edu.mayo.qia.pacs.Notion;
import edu.mayo.qia.pacs.NotionConfiguration;
import edu.mayo.qia.pacs.components.AnonymizationMapProcessor;
import edu.mayo.qia.pacs.components.PoolManager;
import edu.mayo.qia.pacs.dicom.DICOMReceiver;
import edu.mayo.qia.pacs.job.CacheCleaner;
import edu.mayo.qia.pacs.managed.QuartzManager;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.abel", "edu.mayo.qia.pacs.components", "edu.mayo.qia.pacs.dicom", "edu.mayo.qia.pacs.rest"})
@EntityScan(basePackages = {"edu.mayo.qia.pacs.components", "com.abel.pacs.components.entity"})
public class AbelPacsApplication  implements CommandLineRunner {
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private ConfigurableBeanFactory beanFactory;
	
	@Autowired
	private DICOMReceiver dicomReciver;
	
	@Autowired
	private PoolManager poolManager;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private NotionConfiguration notionConfiguration;
	
	private static Logger logger = LoggerFactory.getLogger(AbelPacsApplication.class);
	
	public static void main(String[] args) {
        SpringApplication.run(AbelPacsApplication.class, args);
    }    
    
   

	@Override
	public void run(String... args) throws Exception {
		
		Notion.context = applicationContext; 	
		
		beanFactory.registerAlias("jdbcTemplate", "template");
		
		dicomReciver.start();
		poolManager.start();
		
		 // Start the global Metrics
	    Metrics.register();
		
	    QuartzManager manager = new QuartzManager(StdSchedulerFactory.getDefaultScheduler());
	    manager.start();
	    
	    Scheduler scheduler = manager.scheduler;

	    // Trigger the job to run now, and then repeat every 60 seconds
	    JobDetail job = newJob(AnonymizationMapProcessor.class).withIdentity("cleanup", "alpha").build();
	    Trigger trigger = newTrigger().withIdentity("trigger1", "group1").startNow().withSchedule(simpleSchedule().withIntervalInSeconds(60).repeatForever()).build();

	    // Tell quartz to schedule the job using our trigger
	    scheduler.scheduleJob(job, trigger);

	    job = newJob(CacheCleaner.class).build();
	    trigger = newTrigger().startNow().withSchedule(simpleSchedule().withIntervalInMinutes(10).repeatForever()).build();
	    scheduler.scheduleJob(job, trigger);
	    
	    
	    logger.info("\n\n=====\nStarted Notion Test:\nImageDirectory: \n" + notionConfiguration.notion.imageDirectory + "\nDBWeb:\nhttp://localhost:" + notionConfiguration.dbWeb + "\n\nDICOMPort: " + notionConfiguration.notion.dicomPort + "\n=====\n\n");

	}
}
