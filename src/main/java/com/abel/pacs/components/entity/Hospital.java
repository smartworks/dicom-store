package com.abel.pacs.components.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import edu.mayo.qia.pacs.components.Pool;

@Entity
@Table
@Data
public class Hospital {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	private String name;
			
	private String applicationEntityTitle;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="Poolkey", unique=true, nullable=false, updatable=false)
	private Pool dicomPool;
	
	private String primaryContactName;
	private String primaryContactNumber;
	
	private Integer quotaLimit;
	
	
}
