package com.abel.pacs.components.repository;

import org.springframework.data.repository.CrudRepository;

import com.abel.pacs.components.entity.Hospital;

import edu.mayo.qia.pacs.components.Device;

public interface DeviceRepository extends CrudRepository<Device, Long> {

}
