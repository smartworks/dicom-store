package com.abel.pacs.components.repository;

import org.springframework.data.repository.CrudRepository;

import com.abel.pacs.components.entity.Hospital;

public interface HospitalRepository extends CrudRepository<Hospital, Long> {

}
