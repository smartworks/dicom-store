package com.abel.pacs.components.repository;

import org.springframework.data.repository.CrudRepository;

import edu.mayo.qia.pacs.components.Pool;

public interface PoolRepository extends CrudRepository<Pool, Integer> {

}
