package com.abel.pacs.components.service;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abel.pacs.components.entity.Hospital;
import com.abel.pacs.components.repository.DeviceRepository;
import com.abel.pacs.components.repository.HospitalRepository;
import com.abel.pacs.components.repository.PoolRepository;

import edu.mayo.qia.pacs.components.Device;
import edu.mayo.qia.pacs.components.Pool;
import edu.mayo.qia.pacs.components.PoolManager;
import edu.mayo.qia.pacs.components.User;
import edu.mayo.qia.pacs.db.GroupDAO;
import edu.mayo.qia.pacs.db.GroupRoleDAO;
import edu.mayo.qia.pacs.db.UserDAO;

@Service
@Transactional
public class HospitalService {

	@Autowired
	private HospitalRepository hospitalRepository;

	@Autowired
	private DeviceRepository deviceRepository;

	@Autowired
	private PoolRepository poolRepository;

	@Autowired
	private PoolManager poolManager;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private GroupDAO groupDAO;

	@Autowired
	private GroupRoleDAO groupRoleDAO;
	
	@Autowired
	private SessionFactory sessionFactory;

	public Hospital registerHospital(Hospital hospital) {

		sessionFactory.getCurrentSession();
		
		Pool pool = new Pool();
		pool.applicationEntityTitle = hospital.getApplicationEntityTitle();
		pool.name = hospital.getName();
		pool.description = hospital.getName();
		hospital.setDicomPool(pool);
		hospitalRepository.save(hospital);

		poolManager.newPool(pool);

//		// Create a group, and add this user to it
//		Group g = new Group();
//		g.description = pool.name + " -- Administrators";
//		g.name = "Administrative group for " + pool.name;
//		User user = getCommonUser();
//		g.userKeys.add(user.userKey);
//		groupDAO.create(g);
//		GroupRole gr = new GroupRole();
//		gr.poolKey = pool.poolKey;
//		gr.groupKey = g.groupKey;
//		gr.isPoolAdmin = true;
//		groupRoleDAO.create(gr);

		return hospital;
	}

	public void addDevice(Device device, Long hospitalKey) {
		
		
		Hospital hospital = hospitalRepository.findOne(hospitalKey);
		Pool pool = poolRepository.findOne(hospital.getDicomPool().poolKey);
		
		device.setPool(pool); 		
		
		deviceRepository.save(device);
	}

	private User getCommonUser() {
		String email = "admin@abel.com";
		User user = userDAO.findByEmail(email);
		if (user == null) {
			user = new User();
			user.username = "abel";
			user.password = "password";
			user.activated = true;
			user.email = email;
			return userDAO.create(user);
		} else {
			return user;
		}
	}

}
