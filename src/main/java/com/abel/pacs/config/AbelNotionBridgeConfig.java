package com.abel.pacs.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

import javax.persistence.EntityManagerFactory;

import org.apache.shiro.authz.AuthorizationInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.abel.pacs.notion.NotionServerProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.mayo.qia.pacs.Notion;
import edu.mayo.qia.pacs.NotionConfiguration;
import edu.mayo.qia.pacs.configuration.ServerConfiguration;
import edu.mayo.qia.pacs.db.GroupDAO;
import edu.mayo.qia.pacs.db.GroupRoleDAO;
import edu.mayo.qia.pacs.db.UserDAO;

@Configuration
public class AbelNotionBridgeConfig {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	

	@Autowired
	private NotionServerProperties notionServerConfiguration;

	@Bean
	public Object flyway() {
		return new Object();
	}

	@Bean
	public TaskScheduler taskScheduler() {
		ThreadPoolTaskScheduler s = new ThreadPoolTaskScheduler();
		s.setPoolSize(10);
		return s;
	}

	@Bean
	public HibernateJpaSessionFactoryBean sessionFactory() {
	    return new HibernateJpaSessionFactoryBean();
	}
	
	@Bean
	public UserDAO userDAO() {
		return new UserDAO(sessionFactory().getObject());
	}
	
	@Bean
	public GroupDAO groupDAO() {
		return new GroupDAO(sessionFactory().getObject());
	}
	
	@Bean
	public GroupRoleDAO groupRoleDAO() {
		return new GroupRoleDAO(sessionFactory().getObject());
	}

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

	@Bean
	public Map<String, AuthorizationInfo> authorizationCache() {
		return new ConcurrentHashMap<String, AuthorizationInfo>();
	}	

	@Bean
	public NotionConfiguration configuration() {
		NotionConfiguration notionConfiguration = new NotionConfiguration();

		ServerConfiguration serverconfig = new ServerConfiguration();
		serverconfig.host = notionServerConfiguration.getHost();
		serverconfig.templatePath = notionServerConfiguration.getTemplatePath();
		serverconfig.imageDirectory = notionServerConfiguration.getImageDirectory();
		serverconfig.dicomPort = notionServerConfiguration.getDicomPort();

		notionConfiguration.notion = serverconfig;

		// System.out.println(notionServerConfiguration.getDicomPort());
		return notionConfiguration;
	}
	
	@Bean
	public ExecutorService executor() {
		return Notion.executor;
	}
}
