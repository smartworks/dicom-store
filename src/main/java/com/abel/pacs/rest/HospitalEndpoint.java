package com.abel.pacs.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.abel.pacs.components.entity.Hospital;
import com.abel.pacs.components.service.HospitalService;

import edu.mayo.qia.pacs.components.Device;

@Controller
public class HospitalEndpoint {

	private static final String template = "Hello, %s!";  
	
	@Autowired
	private HospitalService hospitalService;

    @RequestMapping(method=RequestMethod.GET)
    public @ResponseBody String sayHello(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) {
        return String.format(template, name);
    }
    
    @RequestMapping(value="/registerHospital", method = RequestMethod.POST)
    public @ResponseBody Hospital registerHospital(@RequestParam String name, @RequestParam String applicationEntityTitle, @RequestParam String primaryContactName, 
    			@RequestParam String primaryContactNumber, @RequestParam Integer quotaLimit) {  
    	Hospital hospital = new Hospital();
    	hospital.setName(name);
    	hospital.setApplicationEntityTitle(applicationEntityTitle);
    	hospital.setPrimaryContactName(primaryContactName);
    	hospital.setPrimaryContactNumber(primaryContactNumber);
    	hospital.setQuotaLimit(quotaLimit);
    	hospital = hospitalService.registerHospital(hospital);
    	return hospital;     	
    }
    
    @RequestMapping(value="/hospital/addDevice", method = RequestMethod.POST)
    public @ResponseBody Device addDevice(@RequestParam String aet, @RequestParam String hostname, @RequestParam Integer port, @RequestParam Long hospitalKey) {     
    	Device device = new Device(aet, hostname, port);     	
    	hospitalService.addDevice(device, hospitalKey);     	
    	return null;
    	
    }

}
