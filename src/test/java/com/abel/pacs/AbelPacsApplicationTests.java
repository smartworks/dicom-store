package com.abel.pacs;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.abel.pacs.notion.NotionServerProperties;

import edu.mayo.qia.pacs.dicom.DcmSnd;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AbelPacsApplication.class)
@WebAppConfiguration
public class AbelPacsApplicationTests {

	@Autowired
	private WebApplicationContext context;
	
	@Autowired
	private NotionServerProperties notionServerProperties;
	
	private MockMvc mvc;
	
	@Before
	public void setUp() throws Exception {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}
	
	@Test
	public void registerHospitalAndSendImages() throws Exception {

		UUID uid = UUID.randomUUID();
		String aet = uid.toString().substring(0, 10);
		
		// register a hospital
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/registerHospital")
							 .param("name", "palanivel heart")
							 .param("applicationEntityTitle", aet)
							 .param("primaryContactName", "Palanivelrajan")
							 .param("primaryContactNumber", "4045097085")
							 .param("quotaLimit", "10");
		
		MvcResult result = this.mvc.perform(builder).andExpect(status().isOk()).andReturn();
		
		addDevice(1l);
		
		// Now send image.
		
		
		 DcmSnd sender = new DcmSnd("test");
		    // Startup the sender
		 sender.setRemoteHost(notionServerProperties.getHost());
		 sender.setRemotePort(notionServerProperties.getDicomPort());
		 sender.setCalledAET(aet);
		 sender.setCalling(aet);
		
		 List<File> testSeries = getTestSeries("ct.*");
//		 List<File> testSeries = getTestSeries("TOF/*001.dcm");
		    for (File f : testSeries) {
		      if (f.isFile()) {
		        sender.addFile(f);
		      }
		    }
		    sender.configureTransferCapability();
		    sender.open();
		    sender.send(null);
		    sender.close();
		    
							 		
	}
	
	private void addDevice(Long hospitalKey) throws Exception {
		// register a hospital
				MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/hospital/addDevice")
									 .param("aet", ".*")
									 .param("hostname", ".*")
									 .param("port", "1234")
									 .param("hospitalKey", hospitalKey.toString());
				this.mvc.perform(builder).andExpect(status().isOk());
	}
	
	protected List<File> getTestSeries(String resource_pattern) throws IOException {
	    // Load some files
	    PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
	    Resource[] resources = resolver.getResources("classpath:" + resource_pattern);
	    List<File> files = new ArrayList<File>();
	    for (Resource resource : resources) {
	      files.add(resource.getFile());
	    }
	    return files;
	  }

}
